package middlewares

import (
	"strings"

	"github.com/labstack/echo"
)

func HandleJsonURL() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			idx := len(c.ParamNames()) - 1
			if idx < 0 {
				return next(c)
			}

			//fmt.Printf("fix: %#v -> %#v\n", c.ParamNames(), c.ParamValues())
			if paramName := c.ParamNames()[idx]; strings.HasSuffix(paramName, ".json") {
				c.ParamNames()[idx] = paramName[:len(paramName)-5]
			}
			if paramValue := c.ParamValues()[idx]; strings.HasSuffix(paramValue, ".json") {
				c.ParamValues()[idx] = paramValue[:len(paramValue)-5]
			}

			return next(c)
		}
	}
}
