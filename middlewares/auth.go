package middlewares

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/config"
)

type responseAuthFail struct {
	Message string `json:"message"`
}

func CheckAuth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		config := config.GetConfig()

		var tokenString string
		bearToken := c.Request().Header.Get("Authorization")
		bearTokenArr := strings.Split(bearToken, " ")
		if len(bearTokenArr) == 2 {
			tokenString = bearTokenArr[1]
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(config.App.SecretToken), nil
		})
		if err != nil {
			logrus.Error(err)
			return c.JSON(http.StatusUnauthorized, &responseAuthFail{
				Message: "Invalid token",
			})
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			return c.JSON(http.StatusUnauthorized, &responseAuthFail{
				Message: "Invalid token",
			})
		}

		c.Set("shopID", claims["shopID"])
		return next(c)
	}
}
