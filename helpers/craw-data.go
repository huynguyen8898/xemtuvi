package helpers

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"

	"github.com/gocolly/colly"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
)

type Urlset struct {
	XMLUrlSet xml.Name `xml:"urlset"`
	Urls      []Url    `xml:"url"`
}

type Url struct {
	Url xml.Name `xml:"url"`
	Loc string   `xml:"loc"`
}

func ReadSiteMap(sitemap string) (urlSet Urlset) {
	xmlFile, err := os.Open(sitemap)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully Opened users.xml")
	defer xmlFile.Close()
	byteValue, _ := ioutil.ReadAll(xmlFile)
	xml.Unmarshal(byteValue, &urlSet)

	return
}

func VisitLink(urlSet Urlset, db domain.ITraCuuTuViUsecase) {
	for i := 0; i < len(urlSet.Urls); i++ {
		tctv := new(domain.TraCuuTuVi)

		title := urlSet.Urls[i].Loc

		tctv.Title = title
		tctv.Sex = "Nữ"

		c := colly.NewCollector()
		c.OnHTML("#page > #section-content > #zone-content-wrapper > #zone-content", func(e *colly.HTMLElement) {

			content := ""
			e.ForEach(".body-boi-ngay-sinh-theo-bai > p", func(_ int, m *colly.HTMLElement) {
				contentOrigin := regexp.MustCompile(`\n`)
				contentConverted := contentOrigin.ReplaceAllString(m.Text, "<br/>")
				content += "<p>" + contentConverted + "</p>"
			})

			tctv.Body = content

			ctx := context.Background()
			_, err := db.CreateData(ctx, tctv)
			handleError(err)

		})

		c.OnRequest(func(r *colly.Request) {
			fmt.Println("Crawling... ", r.URL)
		})

		c.Visit(urlSet.Urls[i].Loc)
	}
}

func downloadImage(src, title string) (image string) {
	// Tạo hasname cho ảnh tránh bị trùng lặp
	md5HashInBytes := md5.Sum([]byte(title))
	image = hex.EncodeToString(md5HashInBytes[:])
	img, _ := os.Create("img/" + image + ".jpg")
	defer img.Close()

	resp, _ := http.Get(src)
	defer resp.Body.Close()

	b, _ := io.Copy(img, resp.Body)
	fmt.Println("Saved image ! Size: ", b)

	return
}

func handleError(e error) {
	if e != nil {
		panic(e)
	}
}
