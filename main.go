package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/config"
	xemtuviHTTPHandler "gitlab.com/huynguyen.08.08.98/xemtuvi/controllers/http"
	xemtuviRepo "gitlab.com/huynguyen.08.08.98/xemtuvi/controllers/repo"
	xemtuviUsecase "gitlab.com/huynguyen.08.08.98/xemtuvi/controllers/usecase"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/middlewares"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/services/database"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	config := config.GetConfig()

	logrus.SetReportCaller(true)

	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(middlewares.HandleJsonURL())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	// database
	database.NewMongoService() // mongo

	e.GET("/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})

	//repo
	tcdcr := xemtuviRepo.NewMongoTCDCRepo()
	tctvr := xemtuviRepo.NewMongoTCTVRepo()
	// usecase
	tcdcu := xemtuviUsecase.NewThienCanDiaChiUsecase(tcdcr, 60*time.Second)
	tctvu := xemtuviUsecase.NewTraCuuTuViUsecase(tctvr, 60*time.Second)
	//delivery
	xemtuviHTTPHandler.NewTCDCHTTPHandler(e, tcdcu, tctvu)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%v", config.App.Port)))
}
