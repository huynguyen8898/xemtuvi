package usecase

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/domain"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/helpers"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type tctvUsecase struct {
	tctvRepo       domain.ITraCuuTuViRepo
	contextTimeout time.Duration
}

func NewTraCuuTuViUsecase(tctvr domain.ITraCuuTuViRepo, timeOut time.Duration) domain.ITraCuuTuViUsecase {
	return &tctvUsecase{
		tctvRepo:       tctvr,
		contextTimeout: timeOut,
	}
}

func (tctvu *tctvUsecase) CreateData(c context.Context, tctv *domain.TraCuuTuVi) (*domain.TraCuuTuVi, error) {
	ctx, cancel := context.WithTimeout(c, tctvu.contextTimeout)
	defer cancel()

	tcdc, err := tctvu.tctvRepo.Insert(ctx, tctv)

	if err != nil {
		logrus.WithField("Data", fmt.Sprintf("%v", tcdc)).WithError(err).Error("Insert data failed")
		return nil, err
	}

	return tcdc, nil
}

func (tctvu *tctvUsecase) GetTCTV(c context.Context, request *domain.RequestTCTV) (*domain.TraCuuTuVi, error) {
	ctx, cancel := context.WithTimeout(c, tctvu.contextTimeout)
	defer cancel()

	filter := bson.M{}
	year, err := strconv.Atoi(request.Year)
	if err != nil {
		return nil, err
	}

	tcdc := helpers.GetTCDCByYear(year)
	fmt.Println(tcdc)
	filter["name"] = tcdc
	filter["sex"] = request.Sex

	options := options.Find()
	result, err := tctvu.tctvRepo.FindOne(ctx, filter, options)

	if err != nil {
		logrus.WithField("Data", fmt.Sprintf("%v", filter)).WithError(err).Error("Data not found")
		return nil, err
	}

	return result, nil
}
