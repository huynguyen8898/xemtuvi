package database

import (
	"context"
	"fmt"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/huynguyen.08.08.98/xemtuvi/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoService struct {
	Client *mongo.Client
}

var mongoService *MongoService

func NewMongoService() *MongoService {
	if mongoService != nil && mongoService.Client != nil {
		return mongoService
	}

	config := config.GetConfig()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Mongo.Uri))
	if err != nil {
		logrus.WithError(err).Error("Mongo connect fail")
		sentry.CaptureException(err)
		panic(err)
	}

	fmt.Printf("Connect mongo success")

	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		logrus.WithError(err).Error("Ping mongo fail")
		sentry.CaptureException(err)
		panic(err)
	}

	mongoService = &MongoService{
		Client: client,
	}

	return mongoService
}
