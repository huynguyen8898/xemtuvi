package config

import "github.com/spf13/viper"

type configEnv struct {
	App struct {
		Port        string `json:"port"`
		SecretToken string `json:"secretToken"`
	} `json:"app"`

	Mongo struct {
		Uri      string `json:"uri"`
		Database string `json:"database"`
	} `json:"mongo"`
}

var C configEnv

func init() {
	viper.SetConfigFile(`config/config.json`)

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	err = viper.Unmarshal(&C)
	if err != nil {
		panic(err)
	}
}

func GetConfig() configEnv {
	return C
}
